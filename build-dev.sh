cat .env.dev > .env;
cat .data/config/.env >> .env;
cat .data/config/.env.api > projects/api/.env;
cat projects/api/.env.dev >> projects/api/.env;
cat docker-compose.dev.yml > docker-compose.yml;