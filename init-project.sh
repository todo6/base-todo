#!/bin/bash
mkdir .data;
cd .data;
mkdir certbot;
mkdir config;
touch config/.env;
touch config/.env.api;
touch config/.env.webapp;
mkdir mongo;
mkdir webapp;
mkdir webapp/log;

npm install;
mkdir projects;
cd projects;
git clone git@gitlab.com:todo6/api.git
git clone git@gitlab.com:todo6/webapp.git
git clone git@gitlab.com:todo6/mobile.git
