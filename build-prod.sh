cat .env.prod > .env;
cat .data/config/.env >> .env;
cat .data/config/.env.api > projects/api/.env;
cat projects/api/.env.prod >> projects/api/.env;
cat .data/config/.env.webapp > projects/webapp/.env;
cat projects/webapp/.env.prod >> projects/webapp/.env;
cat docker-compose.prod.yml > docker-compose.yml;
